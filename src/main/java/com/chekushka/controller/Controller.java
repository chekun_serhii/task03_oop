package com.chekushka.controller;

import com.chekushka.model.Device;
import com.chekushka.model.rooms.BathDevice;
import com.chekushka.model.rooms.HallDevice;
import com.chekushka.model.rooms.KitchenDevice;

import java.util.*;


public class Controller {
    private static Scanner input = new Scanner(System.in);
    private List<Device> deviceList = new ArrayList<>();

    public final void createList() {
        Device obj1 = new BathDevice("fan", 3, true, "Samsung");
        Device obj2 = new KitchenDevice("toaster", 2, true, "Tefal");
        Device obj3 = new KitchenDevice("Fridge", 8, true, "Samsung");
        Device obj4 = new HallDevice("Tv", 5, true, "Lg");
        deviceList.add(obj1);
        deviceList.add(obj2);
        deviceList.add(obj3);
        deviceList.add(obj4);
    }

    public final void printList() {
        for (int i = 0; i < deviceList.size(); i++) {
            int j = i + 1;
            System.out.println(j + ". " + deviceList.get(i));
        }
    }

    public final void plugIn(final int objNum) {
        deviceList.get(objNum - 1).setActive(true);
    }

    public final void plugOff(final int objNum) {
        deviceList.get(objNum - 1).setActive(false);
    }

    public final double showVoltageSummary() {
        double voltageSum = deviceList.get(0).getVoltage();
        for (int i = 1; i < deviceList.size(); i++) {
            if (deviceList.get(i).isActive()) {
                voltageSum = voltageSum + deviceList.get(i).getVoltage();
            }
        }
        return voltageSum;
    }

    public final void addDevice() {
        System.out.println("Enter the device name:");
        String name = input.next();
        System.out.println("Enter the device voltage(use comma):");
        double voltage = input.nextDouble();
        System.out.println("Enter the device plug in status(true/false):");
        boolean isActive = input.nextBoolean();
        System.out.println("Enter the device manufacturer");
        String manufacturer = input.next();
        System.out.println("Enter the name of device room:\n"
                + "1. Kitchen\n"
                + "2. Bath\n"
                + "3. Hall\n"
                +  "Enter here:");
        int choice = input.nextInt();
        switch (choice) {
            case (1):
                Device newKitchenDevice =
                        new KitchenDevice(name, voltage, isActive, manufacturer);
                deviceList.add(newKitchenDevice);
                break;
            case (2):
                Device newBathDevice =
                        new BathDevice(name, voltage, isActive, manufacturer);
                deviceList.add(newBathDevice);
                break;
            case (3):
                Device newHallDevice =
                        new HallDevice(name, voltage, isActive, manufacturer);
                deviceList.add(newHallDevice);
                break;
            default:
                System.out.println("There is no such room!");
                break;
        }

    }
}

