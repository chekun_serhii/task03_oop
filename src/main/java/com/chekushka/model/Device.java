package com.chekushka.model;

public abstract class Device {
    private String name;
    private double voltage;
    private boolean isActive;
    private String manufacturer;
    private String room;

    public Device(String name, double voltage,
                  boolean isActive, String manufacturer, String room) {
        this.name = name;
        this.voltage = voltage;
        this.isActive = isActive;
        this.manufacturer = manufacturer;
        this.room = room;
    }

    public Device(String name, double voltage,
                  boolean isActive, String manufacturer) {
        this.name = name;
        this.voltage = voltage;
        this.isActive = isActive;
        this.manufacturer = manufacturer;
        this.room = "none";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void plugIn(Device device) {
        setActive(true);
    }

    @Override
    public String toString() {
        return name +
                ", voltage=" + voltage + "v"
                + ", isActive=" + isActive
                + ", manufacturer='" + manufacturer + "\'";
    }
}
