package com.chekushka.model.rooms;

import com.chekushka.model.Device;

public class BathDevice extends Device {
    public BathDevice(String name, double voltage, boolean isActive, String manufacturer, String room) {
        super(name, voltage, isActive, manufacturer, room);
    }

    public BathDevice(String name, double voltage, boolean isActive, String manufacturer){
        super(name, voltage, isActive, manufacturer);
        super.room = "Bath";
    }

}
