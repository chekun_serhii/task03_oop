package com.chekushka.model.rooms;

import com.chekushka.model.Device;

public class HallDevice extends Device {
    public HallDevice(String name, double voltage, boolean isActive, String manufacturer, String room) {
        super(name, voltage, isActive, manufacturer, room);
    }
    public HallDevice(String name, double voltage, boolean isActive, String manufacturer){
        super(name, voltage, isActive, manufacturer);
        super.room = "Hall";
    }

}
