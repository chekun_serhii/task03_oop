package com.chekushka.view;

import com.chekushka.controller.Controller;

import java.util.Scanner;

public class appView {

    private static Scanner input = new Scanner(System.in);
    private Controller controller = new Controller();

    public final void show() {
        while (true) {
            System.out.println("Select an option:\n"
                    + "1. Create device list\n"
                    + "2. Print device list.\n"
                    + "3. Plug in/off device\n"
                    + "4. Show voltage summary\n"
                    + "5. Add Device\n"
                    + "6. Exit");
            System.out.println("Enter here:");
            int choice = input.nextInt();
            switch (choice) {
                case (1):
                    controller.createList();
                    break;
                case (2):
                    controller.printList();
                    break;
                case (3):
                    System.out.println("Enter device number:");
                    int objNum = input.nextInt();
                    System.out.println("Select an option:\n"
                            +"1. Plug in device\n"
                            +"2. Plug off device\n"
                            +"Enter here:");
                    int select = input.nextInt();
                    if (select == 1) {
                        controller.plugIn(objNum);
                    }
                    if (select == 2) {
                        controller.plugOff(objNum);
                    } else {
                        System.out.println("There "
                                + "is no option with this number!");
                    }
                    break;
                case (4):
                    System.out.println("Voltage summary is:" +
                            controller.showVoltageSummary());
                    break;
                case (5):
                    controller.addDevice();
                    break;
                case (6):
                    System.exit(0);
                    break;
                default:
                    System.out.println("There is no option with this number!");
                    break;
            }
        }
    }
}


